#!/bin/bash

NO_WORKERS=3

docker-compose build
docker-compose up --scale worker=$NO_WORKERS