## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
RT-SEC is fully dockerized scalable and versalite data analysis and processing module designed for cybersecurity-oriented SIEM systems. It provides tools for data collection, filtering and analysis. Data is stored as JSON documents inside graph database, featuring entity relationships, powerful AQL language for running complicated queries, including graph travelsals, as well as basic graph visualisation and exploration tools.

## Technologies
Project is created with:
* Python
* Docker
* [ArangoDB](https://www.arangodb.com/)
* [Redis]("https://redis.io/")
* [RQ](https://python-rq.org/)
* [RQ dashboard](https://github.com/Parallels/rq-dashboard)

For integration presentation pourposes open source [Request Tracker for
Incident Response](https://bestpractical.com/rtir) is used.

## Overview
Module overview is available [here](https://gitlab.com/bartoszm.dev/rt-sec/-/wikis/Overview). 

## Setup

### Configuration
Main configuration file is located at app/default.yaml. Configuration is merged with user defined app/config.yaml. Currently only Shodan API key is not set by default config, so it must be set in app/default.yaml under jobs/shodan/api_key key.
```
#config.yaml
    jobs:
        shodan:
        api_key: xxxx
```

### Startup
To run this project, make sure docker service is running and then execute:

```
$ ./run_rtsec.sh
```
