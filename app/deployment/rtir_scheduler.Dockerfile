FROM python:3.8-alpine

WORKDIR /usr/src/app

RUN apk add --no-cache --virtual .build-deps curl gcc musl-dev && \
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    poetry config virtualenvs.create false

COPY ./pyproject.toml ./poetry.lock* /

RUN poetry install --no-root --extras rtir_scheduler && \
    apk --purge del .build-deps

COPY . .
ENV PYTHONPATH /usr/src/app/
CMD ["python", "-u", "services/rtir/scheduler.py"]
