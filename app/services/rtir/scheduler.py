import datetime
import json
import time

import urllib3
from rq import Queue
from rq.exceptions import NoSuchJobError
from rq.job import Job
from rt import Rt, ALL_QUEUES
from rt.exceptions import UnexpectedResponse, ConnectionError

from config import get_config
from processing.job_definitions import get_job
from services.rtir.document_generator import RTIRItemGenerator
from services.utils.periodic_process import PeriodicProcess
from services.utils.queue_utils import get_redis, register_queues


class RTIRScheduler(PeriodicProcess):
    time_format = "%Y-%m-%d %H:%M:%S"
    queue = "RTIR"

    def __init__(self):
        PeriodicProcess.__init__(self)
        self.config = get_config()["RTIR"]
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        self.tracker = Rt(
            url=self.config["url"],
            default_login=self.config["login"],
            default_password=self.config["password"],
            verify_cert=False,
        )
        rt_connected = False
        while not rt_connected:
            try:
                rt_connected = self.tracker.login()
            except (ConnectionError, UnexpectedResponse):
                time.sleep(self.config["connection_timeout"])
                self.logger.error("Can't connect to RTIR")

        self.last_timestamp = self.determine_timestamp()
        self.ticket_ids = None
        self.redis = get_redis()
        register_queues(RTIRScheduler.queue, redis=self.redis)
        self.job_creator = RTIRItemGenerator(self.tracker)

    def get_ticket_last_transaction_time(self, ticket_id: str):
        last_transaction = self.tracker.get_short_history(ticket_id=ticket_id)[-1][0]
        return self.tracker.get_history(
            ticket_id=ticket_id, transaction_id=last_transaction
        )[0]["Created"] + " +{}".format(self.config["timezone"][1:])

    def determine_timestamp(self):
        try:
            with open("./timestamps/{}.json".format(self.__class__.__name__), "r") as f:
                timestamp = json.load(f)["timestamp"]
                self.logger.info("Loaded timestamp from file: {}".format(timestamp))
                return timestamp
        except FileNotFoundError:
            self.logger.info("Timestamp file not found")
        except (json.JSONDecodeError, KeyError):
            self.logger.info("Invalid timestamp file format")

        timestamp = self.config.get("timestamp", None)
        if timestamp is not None:
            self.logger.info("Got timestamp from config")
            return timestamp

        self.logger.info("Setting timestamp to current time")
        return datetime.datetime.today().strftime(
            RTIRScheduler.time_format
        ) + " +{}".format(self.config["timezone"][1:])

    def loop(self):
        self.logger.info("Scheduler pulling changes from RTIR")
        self.pull_changes()
        self.schedule_jobs()
        self.save_timestamp()
        super().loop()

    def pull_changes(self):
        if not self.tracker.login():
            self.logger.error("Scheduler couldn't connect to RTIR")
            raise ConnectionError("Error connecting to RTIR")
        tickets = self.tracker.search(
            Queue=ALL_QUEUES,
            order="-LastUpdated",
            LastUpdated__gt=self.last_timestamp,
            Format="i",
        )

        self.ticket_ids = list(map(lambda x: x["numerical_id"], tickets))
        self.logger.info(
            "Scheduler pulled {} tickets from RTIR".format(len(self.ticket_ids))
        )
        if len(self.ticket_ids) == 0:
            return
        self.last_timestamp = self.get_ticket_last_transaction_time(
            ticket_id=self.ticket_ids[0]
        )

    def schedule_jobs(self):
        self.logger.info("Generating processing.")
        items = self.job_creator.generate_items(tickets=self.ticket_ids)
        self.logger.info(
            "Generated {} processing. Enqueuing.".format(len(items.keys()))
        )
        redis_queue = Queue(name=self.queue, connection=self.redis)
        for doc_key in items.keys():
            item = items[doc_key]
            try:
                job = Job.fetch(id=doc_key, connection=self.redis)
                if job.is_queued or job.is_scheduled:
                    continue
            except NoSuchJobError:
                pass
            job = get_job(item=item, connection=self.redis)
            redis_queue.enqueue_job(job)

    def save_timestamp(self):
        with open("./timestamps/{}.json".format(self.__class__.__name__), "w") as f:
            self.logger.info("Saving last timestamp: {}".format(self.last_timestamp))
            json.dump({"timestamp": self.last_timestamp}, f)

    def cleanup(self):
        self.save_timestamp()
        self.tracker.logout()


if __name__ == "__main__":
    scheduler = RTIRScheduler()
    scheduler.run()
