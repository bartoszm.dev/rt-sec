from typing import List, Dict

from rt import Rt

from config import get_config
from db.arangodb import ArangoDB
from db.documents.rtir import Ticket
from db.entities import Item
from services.rtir.transaction_processor import TransactionProcessor

document_classes = [Ticket]


class RTIRItemGenerator:
    def __init__(self, tracker: Rt):
        self.statuses_to_skip = get_config()["RTIR"]["ignore_status"]
        self.tracker = tracker
        self.tracker.login()
        self.db = ArangoDB(document_classes=document_classes)
        self.current_item: Item = None
        self.transaction_processor = TransactionProcessor(
            tracker=self.tracker, db=self.db
        )

    def generate_items(self, tickets: List[str]) -> Dict[str, Item]:
        items = {}
        for ticket_id in tickets:
            rt_ticket = self.tracker.get_ticket(ticket_id=ticket_id)
            if rt_ticket["Status"] in self.statuses_to_skip:
                continue
            self.current_item = Item(
                document=Ticket.from_rtir_dict(rt_ticket, self.tracker)
            )
            current_ticket = Ticket.from_rtir_dict(rt_ticket, self.tracker)
            items[current_ticket.get_key()] = self.current_item
            transaction_ids = self.determine_new_transactions(ticket=current_ticket)
            items.update(
                self.transaction_processor.process_transactions(
                    transaction_ids=transaction_ids, item=self.current_item
                )
            )
        return items

    def determine_new_transactions(self, ticket: Ticket) -> List[str]:
        db_ticket = self.db.get_document(
            key=ticket.get_key(), collection=ticket.collection
        )
        ticket_history: List[str] = [
            e[0] for e in self.tracker.get_short_history(ticket_id=ticket.get_key())
        ]
        if db_ticket is None or "last_transaction" not in db_ticket:
            return ticket_history
        if (
            db_ticket["last_transaction"] is None
            or db_ticket["last_transaction"] == "none"
        ):
            return ticket_history
        if ticket_history.index(db_ticket["last_transaction"]) == -1:
            return ticket_history
        return ticket_history[ticket_history.index(db_ticket["last_transaction"]) + 1 :]
