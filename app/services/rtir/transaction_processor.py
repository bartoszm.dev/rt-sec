import logging
import re
from enum import Enum
from pprint import pformat
from typing import Dict, Callable, List

from rt import Rt

from db.documents.networking import IP, Domain
from db.documents.rtir import Ticket
from db.entities import Item, Edge


class TransactionType(str, Enum):
    CREATE = "Create"
    CORRESPOND = "Correspond"
    COMMENT = "Comment"
    SUBJECT = "Subject"
    ADD_LINK = "AddLink"
    DELETE_LINK = "DeleteLink"

    @classmethod
    def is_valid(cls, transaction_name: str):
        return transaction_name in cls._value2member_map_


class TransactionProcessor:
    def __init__(self, tracker: Rt, db):
        self.tracker: Rt = tracker
        self.logger = logging.getLogger()
        self.db = db
        self.current_item: Item = None
        self.hooks: Dict[TransactionType, Callable] = {
            TransactionType.ADD_LINK: self.process_new_link,
            TransactionType.CREATE: self.process_text,
            TransactionType.CORRESPOND: self.process_text,
            TransactionType.SUBJECT: self.process_text,
            TransactionType.DELETE_LINK: self.process_unlink,
            TransactionType.COMMENT: self.process_text,
        }

    def process_transactions(
        self, transaction_ids: List[str], item: Item
    ) -> Dict[str, Item]:
        self.current_item = item
        result: Dict[str, Item] = {}
        for transaction_id in transaction_ids:
            transaction = self.tracker.get_history(
                ticket_id=self.current_item.document.get_key(),
                transaction_id=transaction_id,
            )[0]
            if (
                TransactionType.is_valid(transaction["Type"])
                and transaction["Type"] in self.hooks
            ):
                documents = self.hooks[transaction["Type"]](transaction)
                if result is None:
                    continue
                for doc in documents:
                    result[doc.get_key()] = doc.get_item()
        return result

    def process_text(self, transaction: Dict):
        result = []
        result.extend([ip for ip in IP.filter_from_text(transaction["Content"])])
        result.extend(
            [domain for domain in Domain.filter_from_text(transaction["Content"])]
        )
        for doc in result:
            edge = Edge(doc_from=self.current_item.document, doc_to=doc)
            self.current_item.add_new_edge(edge)
        return result

    def process_new_link(self, transaction: Dict):
        self.process_link(transaction)
        return []

    def process_unlink(self, transaction: Dict):
        self.process_link(transaction, new_link=False)
        return []

    def process_link(self, transaction: Dict, new_link=True):
        ticket_id = self.parse_link(description=transaction["Description"])
        if ticket_id is None:
            return
        ticket_dict = self.tracker.get_ticket(ticket_id=ticket_id)
        ticket = Ticket.from_rtir_dict(ticket_dict, tracker=self.tracker)
        if new_link:
            self.current_item.add_new_edge(
                Edge(doc_from=self.current_item.document, doc_to=ticket)
            )
        else:
            self.current_item.add_edge_for_removal(
                Edge(doc_from=self.current_item.document, doc_to=ticket)
            )

    @staticmethod
    def parse_link(description: str):
        match = re.search(r"#\d+", description)
        if match:
            return match.group()[1:]
        else:
            return None

    def log_transaction(self, transaction: Dict):
        self.logger.info(pformat(transaction))
        return []
