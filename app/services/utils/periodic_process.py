import logging
import sched
import signal
import time

from config import get_config


def get_logger():
    """
    Initializes and returns logger instance for usage in docker containers
    :return: initialized logger instance
    """
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(asctime)s- %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    log_level = logging.getLevelName(get_config()["logging"]["level"])
    logger.setLevel(log_level)
    return logger


class PeriodicProcess:
    """Class specifies pattern of behavior for periodically running processes"""
    def __init__(self):
        """
        Initializes PeriodicProcess with logger and scheduler. Registers INT signal handling for cleanup methods.
        """
        self.interrupted = False
        config = get_config()["periodic_process"]
        self.status_refresh_interval = config["refresh_interval"]
        self.pooling_delay = config["pooling_delay"]
        self.scheduler = sched.scheduler(time.time, time.sleep)
        self.current_task = None
        self.logger = get_logger()

        signal.signal(signal.SIGTERM, self.set_interrupted)

    def run(self):
        """
        Entry method for PeriodicProcess, starts loop and check if interrupted every pooling_delay secs
        """
        self.current_task = self.scheduler.enter(
            self.status_refresh_interval, 1, self.loop
        )
        while not self.interrupted:
            next_ev = self.scheduler.run(blocking=False)
            if next_ev is not None:
                time.sleep(min(self.status_refresh_interval, next_ev))
            else:
                time.sleep(self.status_refresh_interval)

    def loop(self):
        """
        Loop instructions needed to be run at the end of each loop
        """
        if not self.interrupted:
            self.current_task = self.scheduler.enter(self.pooling_delay, 1, self.loop)

    def cleanup(self):
        raise NotImplementedError

    def set_interrupted(self, signum, _frame):
        """
        INT signal handler, cancels current scheduler task, performs cleanup and sets interrupted flag.
        """
        self.cleanup()
        if not self.scheduler.empty():
            self.scheduler.cancel(self.current_task)
        if not self.interrupted:
            self.interrupted = True
            self.logger.info(
                "{} received signal {}, exiting.".format(
                    self.__class__.__name__, signum
                )
            )
