from typing import Union, List

from redis import Redis

from config import get_config


def get_redis():
    """
    Returns redis instance based on config
    :return: redis instance
    """
    config = get_config()
    redis_host = config["redis"]["host"]
    redis_port = config["redis"]["port"]
    return Redis.from_url(
        "redis://{}:{}/0".format(redis_host, redis_port), socket_connect_timeout=10
    )


def get_queues(redis: Redis = None):
    """
    Returns list of registered queues
    :param redis:
    :return: List of queue names
    """
    if redis is None:
        redis = get_redis()
    if not redis.exists("Queues"):
        return []
    return [q.decode("utf-8") for q in redis.lrange("Queues", 0, -1)]


def register_queues(queues: Union[str, List[str]], redis: Redis = None):
    """
    Registers queues to redis, prevents duplicates
    :param queues: either one queue name or list of names
    :param redis: redis instance
    """
    queues = queues if type(queues) == list else [queues]
    if redis is None:
        redis = get_redis()
    for q in queues:
        if q not in get_queues(redis=redis):
            redis.lpush("Queues", q)
