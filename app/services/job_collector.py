from typing import Dict

from rq import Queue
from rq.exceptions import NoSuchJobError
from rq.job import Job
from rq.registry import FinishedJobRegistry

from db.arangodb import ArangoDB
from db.entities import Item
from processing.job_definitions import get_job, job_definitions
from services.utils.periodic_process import PeriodicProcess
from services.utils.queue_utils import get_redis, get_queues


class JobCollector(PeriodicProcess):
    """Class defining JobCollector, which commits finished job items to database and enqueues new jobs based on feedback documents"""
    def __init__(self):
        PeriodicProcess.__init__(self)
        self.finished_jobs = []
        self.db = ArangoDB(document_classes=job_definitions.keys())
        self.current_queue: Queue = None
        self.redis = get_redis()

    def loop(self):
        """
        Inside loop JobCollector fetches registered job queues and for each of them process finished jobs
        """
        queues = get_queues(redis=self.redis)
        for q in queues:
            self.logger.info("Job collector pulling results from {}".format(q))
            self.current_queue = Queue(name=q, connection=self.redis)
            self.process_jobs()
        super().loop()

    def process_jobs(self):
        """
        When processing jobs, JobCollector pulls items out of finished jobs, filters feedback documents to prevent duplicates, enqueues feedback jobs and at the end commits items to database"
        """
        items = []
        feedback_items: Dict[str, Item] = {}

        finished_job_registry = FinishedJobRegistry(
            queue=self.current_queue, connection=self.redis
        )
        finished_job_ids = finished_job_registry.get_job_ids()
        if finished_job_ids == 0:
            self.logger.info("Finished job registry is empty, skipping.")
            return

        for job_id in finished_job_ids:
            job = Job.fetch(job_id, connection=self.redis)
            self.logger.info("Fetching job {}.".format(job.id))
            feedback_items.update(self.process_feedback_documents(job.result))
            items.append(job.result)
            job.delete(remove_from_queue=True)

        self.logger.info(
            "Found {} feedback items. Enqueuing processing.".format(len(feedback_items))
        )

        for item in feedback_items.values():
            self.current_queue.enqueue_job(get_job(item=item, connection=self.redis))

        self.logger.info("Committing {} items to database.".format(len(items)))
        self.db.commit_items(items)

    def process_feedback_documents(self, item: Item) -> Dict[str, Item]:
        """
        Processes Item's feedback documents and for each document makes sure it doesn't exist in database nor is processed right now
        :param item: item to be processed
        :return: dict with document keys as keys and created items as values
        """
        result: Dict[str, Item] = {}
        for doc in item.get_feedback_documents().values():
            try:
                _ = Job.fetch(id=doc.get_key(), connection=self.redis)
                continue
            except NoSuchJobError:
                pass
            if self.db.get_vertex_collection(doc.collection).has(doc.get_key()):
                continue
            result[doc.get_key()] = doc.get_item()
        return result

    def cleanup(self):
        pass


if __name__ == "__main__":
    job_collector = JobCollector()
    job_collector.run()
