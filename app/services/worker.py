"""Module implementing worker initialization and startup, pulls registered queues and loads config, then starts worker"""

import os
import time

from rq import Connection, Worker

from config import get_config
from services.utils.queue_utils import get_redis, get_queues

with Connection(get_redis()):
    time.sleep(get_config()["worker"]["delay"])
    qs = os.getenv("QUEUES", get_queues())
    if qs is None or len(qs) == 0:
        qs = ["default"]

    w = Worker(qs)
    w.work()
