import os

import hiyapyco


def get_config():
    if not os.path.isfile("default.yaml"):
        raise FileNotFoundError("Missing config file default.yaml")
    return hiyapyco.load(
        "default.yaml",
        "config.yaml",
        method=hiyapyco.METHOD_MERGE,
        interpolate=True,
        failonmissingfiles=False,
    )
