from typing import List

from db.entities import Document
from processing.filters.networking import filter_ips, filter_domains


class Domain(Document):
    """Class representing domain name"""
    @classmethod
    def filter_from_text(cls, text: str):
        """
        Filters domain names from URL contained in provided piece of text.
        :param text: text to be searched
        :return: List of Domain objects representing unique domain names
        """
        return [Domain(_key=domain) for domain in filter_domains(text)]


class IP(Document):
    """Class representing IPv4 or IPv6 address"""
    @classmethod
    def filter_from_text(cls, text: str) -> List:
        """
        Filters IP addresses from provided piece of text.
        :param text: text to be searched
        :return: List of IP objects representing unique ip addresses
        """
        return [IP(_key=ip) for ip in filter_ips(text)]
