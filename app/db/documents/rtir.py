from db.entities import Document

__all__ = ["IncidentReport", "Investigation", "Incident", "Ticket"]


class Ticket(Document):
    skip = True

    def __init__(self, ticket_id: str, last_transaction: str = None):
        super().__init__(_key=ticket_id)
        self.data["queue"] = self.collection
        self.data["last_transaction"] = last_transaction
        self.use_as_feedback = False
        self.collection = "Ticket"

    def get_queue(self) -> str:
        return self.data["queue"]

    @classmethod
    def from_rtir_dict(cls, rt_ticket: dict, tracker):
        if rt_ticket["Queue"] == "Incidents":
            ticket = Incident(ticket_id=rt_ticket["numerical_id"])
        elif rt_ticket["Queue"] == "Incident Reports":
            ticket = IncidentReport(ticket_id=rt_ticket["numerical_id"])
        else:
            ticket = Investigation(ticket_id=rt_ticket["numerical_id"])
        ticket_data = ticket.get_data()
        ticket_data["last_transaction"] = tracker.get_short_history(
            ticket_id=ticket.get_key()
        )[-1][0]
        ticket_data["subject"] = rt_ticket["Subject"]
        ticket_data["created"] = rt_ticket["Created"]
        ticket_data["creator"] = rt_ticket["Creator"]
        return ticket


class Incident(Ticket):
    pass


class IncidentReport(Ticket):
    pass


class Investigation(Ticket):
    pass
