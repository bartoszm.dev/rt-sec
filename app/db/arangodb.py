import logging
import time
from datetime import datetime
from typing import Union, Sequence, List, Tuple

from arango import ArangoClient
from arango.exceptions import ServerConnectionError

from config import get_config
from db.entities import Item, Edge, Document


class ArangoDB:
    """Class providing interface for interacting with ArangoDB"""

    def __init__(self, document_classes: Sequence = None):
        """

        :param document_classes: List of document classes used to define new vertex collections
        """
        self.logger = logging.getLogger()
        self.config = get_config()["arangodb"]
        self.db_name = self.config["db"]["name"]
        self.db_username = self.config["db"]["username"]
        self.db_password = self.config["db"]["password"]
        self.graph_name = self.config["graph_name"]
        self.remove_orphans = self.config["remove_orphans"]
        self.client = ArangoClient(hosts=self.config["url"])
        self.db = None
        self.graph = None
        self._initialize_database()
        if document_classes is not None:
            self._initialize_graph(document_classes)

    def commit_items(self, items: Union[Item, List[Item]]):
        """
        Commits given Item or Items to database
        :param items: either Item or list of Items
        """
        items: List[Item] = items if type(items) == list else [items]
        self.logger.info("Commiting {} documents.".format(len(items)))
        edges = []
        for item in items:
            self.commit_document(item.document)
            edges.extend(item.get_edges())
        self.logger.info("Updating edges connected to committed documents")
        self.commit_edges(edges)

    def commit_edges(self, edges: Union[Edge, List[Tuple[bool, Edge]]]):
        """
        Either adds or removes edges based on given tuple, edge collections are created on demand
        :param edges: either edge or list of edges
        """
        edges = edges if type(edges) == list else [edges]
        for (new_edge, edge) in edges:
            if new_edge:
                self.add_edge(edge)
            else:
                self.remove_edge(edge)

    def commit_document(self, document: Document):
        """
        Either updates/replaces existing document or adds new
        :param document: Document to be committed
        :return: dictionary representing document after committing
        """
        if document is None:
            return
        collection = self.get_vertex_collection(document.collection)
        if collection is None:
            self.logger.info(
                "Document collection {} does not exist, skipping removal of edge {}.".format(
                    collection, document.get_key()
                )
            )
            return
        document.data["last_updated"] = datetime.now()
        if collection.has(document.get_key()):
            if document.replace:
                collection.replace(document.get_normalized_data())
            else:
                collection.update(document.get_normalized_data())
        else:
            collection.insert(document.get_normalized_data())
        return collection.get(document.get_key())

    def get_document(self, collection: str, key: str):
        return self.get_vertex_collection(collection).get(key)

    def _initialize_database(self):
        system_db = None
        arangodb_connected = False
        while not arangodb_connected:
            try:
                system_db = self.client.db(
                    name="_system",
                    username="root",
                    password=self.config["root"]["password"],
                    verify=True,
                )
                arangodb_connected = True
            except ServerConnectionError:
                time.sleep(3)
                self.logger.error("Can't connect to ArangoDB")

        if not system_db.has_database(self.db_name):
            self.logger.info("Creating {} database".format(self.db_name))
            system_db.create_database(name=self.db_name)

        if not system_db.has_user(username=self.db_username):
            self.logger.info("Creating user {}".format(self.db_username))
            system_db.create_user(username=self.db_username, password=self.db_password)

        if (
            system_db.permission(username=self.db_username, database=self.db_name)
            != "rw"
        ):
            self.logger.info("Setting permissions")
            system_db.update_permission(
                username=self.db_username, permission="rw", database=self.db_name
            )

        self.db = self.client.db(
            name=self.db_name, username=self.db_username, password=self.db_password
        )

        if not self.db.has_graph(self.graph_name):
            self.logger.info("Creating graph")
            self.db.create_graph(self.graph_name)

    def _initialize_graph(self, document_classes: Sequence):
        self.graph = self.get_database().graph(self.graph_name)
        self.logger.info("Initializing [{}] graph".format(self.graph.name))
        self.logger.info(
            "Document types: {}. Creating vertex collections".format(
                ", ".join([cls.__name__ for cls in document_classes])
            )
        )
        for cls in document_classes:
            if not self.graph.has_vertex_collection(cls.__name__):
                self.graph.create_vertex_collection(cls.__name__)

    def get_database(self):
        return self.client.db(
            self.db_name,
            username=self.db_username,
            password=self.db_password,
            verify=True,
        )

    def get_vertex_collection(self, name: str):
        graph = self.get_database().graph(self.graph_name)
        if graph.has_vertex_collection(name):
            return graph.vertex_collection(name)
        return None

    def get_edge_collection(self, name: str):
        graph = self.get_database().graph(self.graph_name)
        if graph.has_edge_definition(name):
            return graph.edge_collection(name)
        return None

    def add_edge_collection(self, edge: Edge):
        """
        Adds new edge collection based on given example Edge object
        :param edge:
        """
        self.logger.info(
            "Adding edge collection: {}".format(edge.get_edge_collection())
        )
        self.graph.create_edge_definition(
            edge_collection=edge.get_edge_collection(),
            from_vertex_collections=[edge.from_collection],
            to_vertex_collections=[edge.to_collection],
        )

    def add_edge(self, edge: Edge):
        """
        Adds new edge, edge collections are created on the go
        :param edge: edge to be added
        """
        if not self.graph.has_edge_definition(edge.get_edge_collection()):
            self.add_edge_collection(edge)
        edge_col = self.get_edge_collection(edge.get_edge_collection())
        if not (self.get_vertex_collection(edge.to_collection).has(edge.to_key)):
            doc = edge.doc_to_class(_key=edge.to_key)
            self.commit_document(document=doc)
        if not (self.get_vertex_collection(edge.from_collection).has(edge.from_key)):
            doc = edge.doc_from_class(_key=edge.from_key)
            self.commit_document(document=doc)
        if not (edge_col.has(edge.get_key())):
            self.logger.info("Adding new link: {}".format(edge.get_key()))
            edge_col.link(edge.get_from(), edge.get_to(), data=edge.data)

    def remove_edge(self, edge: Edge):
        """
        Removes given edge, if edge not found returns with no error.
        When remove_orphans is set, vertices which have no related edges after removal are removed too.
        :param edge: edge to be removed
        """
        edge_col = self.get_edge_collection(edge.get_edge_collection())
        if edge_col is None:
            self.logger.info(
                "Edge collection {} does not exist, cannot remove edge {}. Skipping.".format(
                    edge.get_edge_collection(), edge.get_key()
                )
            )
            return
        if edge_col.has(edge.get_key()):
            edge_col.delete(edge.get_key())
        if not self.remove_orphans:
            return
        if len(edge_col.edges(edge.to_key).keys()) == 0:
            self.get_vertex_collection(name=edge.to_collection).delete(edge.to_key)
