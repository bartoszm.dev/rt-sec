import json
from typing import List, Dict, Union, Tuple

from config import get_config


def simple_pass(item):
    return item


class Document:
    """Class representing single document (graph vertex) stored in database."""


    available_links: List = []

    def __init__(
        self,
        _key: str,
        collection: str = None,
        data: dict = None,
        replace: bool = False,
    ):
        """
        Sets all attributes for Document object
        :param _key: Unique identifier of Document
        :param collection: Document's vertex collection name
        :param data: it is what will be saved to database, certain fields are overwritten like _key
        :param replace: defines if this document should replace one in databse on save
        """

        config = get_config()["documents"]

        if collection is None:
            self.collection = self.__class__.__name__
        else:
            self.collection = collection

        if data is None:
            data = {}
        self.data = data
        self.data["_key"] = _key
        self.data["_fid"] = "{} [{}]".format(self.get_key(), self.__class__.__name__)
        color = config.get(self.__class__.__name__, {"color": None})["color"]
        self.data["_color"] = color

        self.replace = replace

    def get_item(self):
        """
        Creates new Item object with current document

        :return: Item with current document as main document
        """
        return Item(document=self)

    def get_key(self):
        return self.data["_key"]

    def get_collection(self):
        return self.collection

    def get_data(self):
        return self.data

    def get_normalized_data(self):
        """
        Returns Document's data in fully JSON serializable form
        :return:
        """
        return json.loads(json.dumps(self.data, default=str))


class Edge:
    """Class representing edge connecting two documents in graph"""
    def __init__(
        self,
        doc_from: Document = None,
        doc_to: Document = None,
        data: dict = None,
    ):
        """
        Constructs Edge object from provided parameters

        :param doc_from: Document object from which link points
        :param doc_to: Document object being pointed by link
        :param data: additional edge data saved in database
        """
        if doc_from is not None:
            self.doc_from_class = doc_from.__class__
            self.from_key = doc_from.get_key()
            self.from_collection = doc_from.collection
            self.from_class = doc_from.__class__

        if doc_to is not None:
            self.doc_to_class = doc_to.__class__
            self.to_key = doc_to.get_key()
            self.to_collection = doc_to.collection
            self.edge_name = doc_to.__class__.__name__


        if (
            self.to_collection is None
            or self.from_collection is None
            or self.to_key is None
            or self.from_key is None
        ):
            raise AttributeError(
                "From and to documents' collections and keys must be specified when creating Edge"
            )

        if data is None:
            data = {}
        self.data = data
        self.data = {
            "_key": "{}::{})-({}::{}".format(
                self.from_collection, self.from_key, self.to_key, self.to_collection
            )
        }

    def get_from(self):
        """
        Returns Edge's from Document's _id: collection_name/document_key
        :return: string containing from Document's _id
        """
        return "{}/{}".format(self.from_collection, self.from_key)

    def get_to(self):
        """
        Returns Edge's to Document's _id: collection_name/document_key
        :return: string containing to Document's _id
        """
        return "{}/{}".format(self.to_collection, self.to_key)

    def get_key(self):
        return self.data["_key"]

    def get_edge_collection(self) -> str:
        return self.to_collection.lower()


class Item:
    """Class representing Item, which is processed by workers.
    Contains main document, related edges and feedback documents"""

    def __init__(
        self,
        document: Document,
        edges: List[Tuple[bool, Edge]] = None,
    ):
        """
        Constructs Item from provided arguments
        :param document: Item's main document
        :param edges: list of tuples, in which first item (boolean) defines if edges is new (True) or for removal (False), and second item is the Edge
        """
        self.document = document
        if edges is None:
            edges = []

        self._edges: List[Tuple[bool, Edge]] = edges
        self._feedback_documents: Dict[str, Document] = {}

    def add_feedback_documents(self, documents: Union[Document, List[Document]]):
        """
        Adds feedback documents, performs checks so no duplicates are allowed
        :param documents: either Document or list of Documents
        """
        documents = documents if type(documents) == list else [documents]
        for doc in documents:
            self._feedback_documents[doc.get_key()] = doc

    def add_new_edge(self, edge: Edge):
        self._edges.append((True, edge))

    def add_edge_for_removal(self, edge: Edge):
        self._edges.append((False, edge))

    def get_edges(self):
        return self._edges

    def get_feedback_documents(self):
        return self._feedback_documents
