import pytest

from processing.filters.networking import filter_ips, filter_domains

IPV4_TEST_TEXT = """Suspicious activity originating from following
 IP addresses: 123.23.32.21. Another activity from 111[.]222.234.255.
 Invalid activities from 1234.123.123.123 and 123[.]256.12.121 and 123.123.123.1234.
 Another activity from private addresses 192.168.1.1 and 192[.]168.100.12"""

IPV6_TEST_TEXT = (
    """1762:0:0:0:0:B03:1:AF18, testing1212.222, FE80::903A:1C1A:E802:11E4"""
)

DOMAIN_TEST_TEXT = """Asdfmovie malicious activity at http://google.com/search and https://wp.pl/asdf.
                   Phishing targeting http://www.netflix.asd.as-as.com/afk at hxxp://nflix.om and hxxps://flix.se
                   Another masked url at http://onet[.]pl/fff also invalid domain names: http://-asd.pl,
                   http://as_de.pl and http://as_de.As"""


class TestIPV4Filter:
    @pytest.fixture(scope="class")
    def ipv4_result(self):
        return filter_ips(IPV4_TEST_TEXT)

    def test_filtering_standard_ipv4_addresses(self, ipv4_result):
        assert "123.23.32.21" in ipv4_result

    def test_filtering_masked_ipv4_addresses(self, ipv4_result):
        assert "111.222.234.255" in ipv4_result

    def test_filtering_invalid_ipv4_addresses(self, ipv4_result):
        assert "1234.123.123.123" not in ipv4_result
        assert "123.256.12.121" not in ipv4_result
        assert "123.123.123.123" not in ipv4_result

    def test_filtering_private_ipv4_addresses(self, ipv4_result):
        assert "192.168.1.1" not in ipv4_result
        assert "192.168.100.12" not in ipv4_result


class TestIPV6Filter:
    @pytest.fixture(scope="class")
    def ipv6_result(self):
        return filter_ips(IPV6_TEST_TEXT)

    def test_filtering_standard_ipv4_addresses(self, ipv6_result):
        assert "1762:0:0:0:0:B03:1:AF18" in ipv6_result

    def test_filtering_private_ipv4_addresses(self, ipv6_result):
        assert "FE80::903A:1C1A:E802:11E4" not in ipv6_result


class TestDomainFilter:
    @pytest.fixture(scope="class")
    def domain_result(self):
        return filter_domains(DOMAIN_TEST_TEXT)

    def test_http_filter(self, domain_result):
        assert "google.com" in domain_result

    def test_https_filter(self, domain_result):
        assert "wp.pl" in domain_result

    def test_www_prefix_filter(self, domain_result):
        assert "netflix.asd.as-as.com" in domain_result

    def test_hxxp_mask_filter(self, domain_result):
        assert "nflix.om" in domain_result

    def test_hxxps_mask_filter(self, domain_result):
        assert "flix.se" in domain_result

    def test_bracket_masked_filter(self, domain_result):
        assert "onet.pl" in domain_result

    def test_invalid_domain_filter(self, domain_result):
        assert "http://-asd.pl" not in domain_result
        assert "http://as_de.pl" not in domain_result
        assert "http://as_de.As" not in domain_result
