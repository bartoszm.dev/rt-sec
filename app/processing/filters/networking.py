import ipaddress
import re
from typing import Set

IP_V4_REGEX = re.compile(
    r"\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}"
    r"(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b"
)

IP_V6_REGEX = re.compile(r"(?<![:.\w])(?:[A-F0-9]{1,4}:){7}[A-F0-9]{1,4}(?![:.\w])")

URL_REGEX = re.compile(r"\bhttps?://[a-zA-Z0-9]+[a-zA-Z0-9\-.]*[a-z]+")


def check_if_global_ipv4(ip: str):
    """
    Checks if provided IPv4 address is global (nor private or restricted)
    :param ip: IPv4 address
    :return: True if global, False otherwise
    """
    try:
        return ipaddress.IPv4Address(ip).is_global
    except ipaddress.AddressValueError:
        return False


def check_if_global_ipv6(ip: str):
    """
    Checks if provided IPv6 address is global (nor private or restricted)
    :param ip: IPv6 address
    :return: True if global, False otherwise
    """
    try:
        return ipaddress.IPv6Address(ip).is_global
    except ipaddress.AddressValueError:
        return False


def filter_ips(text: str) -> Set[str]:
    """
    Filter unique global IP addresses (v4 and v6) from text.
    Filters masked addressed too.
    :param text: text to be filtered
    :return: list of IP addresses as strings
    """
    prepared_text = re.sub(r"\[.]", ".", text)
    v4_ips = set(re.findall(IP_V4_REGEX, prepared_text))
    v6_ips = set(re.findall(IP_V6_REGEX, prepared_text))
    ips = set(filter(check_if_global_ipv4, v4_ips))
    ips.update(set(filter(check_if_global_ipv6, v6_ips)))
    return ips


def filter_domains(text: str) -> Set[str]:
    """
    Filters domain names from URL contained in text including masked URLs
    :param text: text to be filtered
    :return: list of unique domain names
    """
    prepared_text = re.sub(r"hxxp", "http", re.sub(r"\[.]", ".", text))
    urls = set()
    for url in re.findall(URL_REGEX, prepared_text):
        url: str = url[url.index("/") + 2 :]
        url = url[4:] if url.startswith("www.") else url
        urls.add(url)
    return urls
