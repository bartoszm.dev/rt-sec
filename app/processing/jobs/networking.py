from config import get_config
from db.entities import Item, Edge

config = get_config()["jobs"]

def process_ip(item: Item):
    """
    Basic IP address processing including RDAP queries and Shodan host check (if Shodan API key set)
    :param item: Item containing IP as main document
    :return: item with updated IP doc
    """
    from ipwhois import IPWhois
    import shodan

    ip = item.document.get_key()
    data = IPWhois(ip).lookup_rdap(config["process_ip"]["rdap_depth"])
    data.pop("query")
    item.document.data["RDAP"] = data
    shodan_api_key = config.get("shodan", "{}").get("api_key", None)
    if shodan_api_key is not None:
        try:
            api = shodan.Shodan(shodan_api_key)
            item.document.data["Shodan"] = api.host(ip)
        except shodan.exception.APIError:
            pass
    return item


def process_domain(item: Item):
    """
    Basic domain name processing including whois query and nslookup creating feedback IP document
    :param item: item containing Domain as main document
    :return: item with updated Domain doc and possibly new feedback IP docs
    """
    import whois
    import socket
    from db.documents import IP

    domain = item.document.get_key()
    try:
        item.document.data.update(whois.query(domain).__dict__)
        ip = IP(_key=socket.gethostbyname(item.document.get_key()))
        item.add_feedback_documents(ip)
        item.add_new_edge(Edge(doc_from=item.document, doc_to=ip))
    except (whois.FailedParsingWhoisOutput, whois.UnknownDateFormat, whois.UnknownTld):
        pass
    return item
