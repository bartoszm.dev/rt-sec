"""Module containing job definitions specified in job_definitions dict.
Dict's keys are item's main document classes and values are functions to be executed for these items"""

from redis import Redis
from rq.job import Job

from db.documents.networking import IP, Domain
from db.entities import Item
from processing.jobs.networking import process_ip, process_domain

job_definitions = {IP: process_ip, Domain: process_domain}


def simple_pass(item: Item):
    """
    The most simple job, just passing Item to job collector
    :param item: item to be passed
    :return: passed item
    """
    return item


def get_job(item: Item, connection: Redis):
    """
    Function providing job creation for given item and redis connection
    :param item: item for which job is created
    :param connection: redis instance
    :return: created Job object
    """
    document_class = item.document.__class__
    if document_class in job_definitions:
        func = job_definitions[document_class]
    else:
        func = simple_pass
    return Job.create(
        func, id=item.document.get_key(), connection=connection, kwargs={"item": item}
    )
